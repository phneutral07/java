/**
 * Beschreiben Sie hier die Klasse Spielesammlung.
 * 
 * @author (Limbeck) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Spielesammlung
{
    private String nameSpielesammlung;
    private Wuerfel wuerfel;
    
    /**
     * Konstruktor für Objekte der Klasse Spielesammlung
     */
    public Spielesammlung()
    {
        nameSpielesammlung = "Kepler Games";
        wuerfel = new Wuerfel();     
    }

    /**
     * DreierPasch: Es wird ueber die Konsole ausgegeben, ob ein 3er Pasch gewuerfelt wurde oder nicht
     */
    public void spielenDreierPasch_OhneArray()
    {
        
        int hWert1 = wuerfel.wuerfeln();
        int hWert2 = wuerfel.wuerfeln();
        int hWert3 = wuerfel.wuerfeln();

        if(hWert1 == hWert2 && hWert1 == hWert3)
        {
            System.out.println("Pasch mit der Augenzahl: " + hWert1 + " !!");
        }
        else
        {
            System.out.println("Leider kein Pasch. Du hast " + hWert1 + ", " + hWert2 + " und " + hWert3 + " gewuerfelt.");
        }               
    }     
    
    /**
     * DreierPasch: Es wird zurueck gegeben, ob ein 3er Pasch gewuerfelt wurde oder nicht
     */
    public boolean spielenDreierPasch()
    {
        boolean istPasch = false;  
        return istPasch;
    } 
    
    /**
     * Etwas komplexer:
     * Wie oft muss gewuerfelt werden, um ein x-pasch zu werfen, 
     * z.B. dreier, vierer oder fuenfer Pasch.
     */
    public int xPasch_wieOft(int pX)
    {           
        int wurfzahl = 0;
        int[] wuerfe = new int[3];
        while(pX != wuerfe[0] || pX != wuerfe[1] || pX != wuerfe[2]) 
        {
            for(int i=0;i<wuerfe.length;i++)
            {
                wuerfe[i] = wuerfel.wuerfeln();
                wurfzahl++;
                System.out.println(wuerfe[i]);
            }
        }
        return wurfzahl;
    } 
    public boolean testeWuerfel()
    {
        boolean istKorrekt = false;
        boolean istFalsch = false;
        int auge = 0;
        for(int i = 0; i < 100; i++)
        {
            auge = wuerfel.wuerfeln();
            if(auge <= wuerfel.getAnzahlSeiten() && auge > 0)
            {
                istKorrekt = true;
            }
            else
            {
                istFalsch = true;
            }
        }
        System.out.println("Ist der Würfel falsch? " + istFalsch);
        return istFalsch;
    }
    public int spielenDreierPasch_automatisch()
    {
        int[] wuerfe = new int[3];
        int wurfzahl = 3;
        
        for(int i = 0; i < wuerfe.length; i++)
        {
            wuerfe[i] = wuerfel.wuerfeln();
        }
        
        while(wuerfe[0] != wuerfe[1] || wuerfe[0] != wuerfe[2]) 
        {
            for(int i=0;i<wuerfe.length;i++)
            {
                wuerfe[i] = wuerfel.wuerfeln();
                wurfzahl++;
                System.out.println(wuerfe[i]);
            }
        }
        return wurfzahl;
    }
    
    public int spielenDreierPaschstatistik()
    {
        int durchschnitt = 0;
        for(int i = 0; i < 100; i++)
        {
            durchschnitt += spielenDreierPasch_automatisch();
        }
        durchschnitt /= 100;
        return durchschnitt;
    }
    public void spielenBlackjack(int pObergrenze)
    {
        int hAnzahlWuerfe = 0;   //Sprinter
        int hAugenzahl = 0;      
        int hSumme = 0;

        while(hSumme <= pObergrenze) 
        {                            
            hSumme = hSumme + wuerfel.wuerfeln();
            System.out.println("Aktuelle Summe: " + hSumme);
        }

        if(hSumme == 21)
        { 
            System.out.println("Glueckwunsch, du bist genau auf 21 gekommen!");
        }
        else if(hSumme > 21)
        {
            System.out.println("Du hast leider verloren!");
        }
        else
        {
            System.out.println("Aktuelle Summe: " + hSumme);
        }       
    }   

    /**
     * Blackjack gegen den Computer
     * @param pObergrenze Summe, bei der noch einmal gewürfelt wird.
     */
    public void blackjackGegenRechner(int pObergrenze)
    {
        int hPunkte = 0;

        while(hPunkte<=pObergrenze) // zuerst wuerfelt der Spieler
        {
            hPunkte += wuerfel.wuerfeln();
        }

        if(hPunkte==21)
        {
            System.out.println("Du hast mit 21 Punkten gewonnen!!!");
        }
        else if(hPunkte>21)
        {
            System.out.println("Du hast leider eine zu hohe Summe gewuerfelt und bist auf "+hPunkte+" Punkte gekommen. Verloren!");
        }
        else // Falls der Spieler < 21 Punkte hat, "wuerfelt" der Computergegner
        {
            int hPunkteRechner = 0;

            while(hPunkteRechner<=hPunkte)
            {
                hPunkteRechner += wuerfel.wuerfeln();
            }

            if(hPunkteRechner == 21)
            {
                System.out.println("Du hast mit "+hPunkte+" Punkten verloren. (Dein Gegner hat "+hPunkteRechner+" Punkte gewuerfelt)");

            } else{
                System.out.println("Du hast mit "+hPunkte+" Punkten gewonnen! (Dein Gegner hat "+hPunkteRechner+" Punkte gewuerfelt)");
            }
        }                
    }    
}

