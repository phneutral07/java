

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Write a description of JavaFX class FX here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class FX extends Application
{
    // We keep track of the count, and label displaying the count:
    private Label myLabel = new Label("0");
    private Label myLabel2 = new Label("0");
    private Spielesammlung spielesammlung;
    private Wuerfel wuerfel;
    /**
     * The start method is the main entry point for every JavaFX application. 
     * It is called after the init() method has returned and after 
     * the system is ready for the application to begin running.
     *
     * @param  stage the primary stage for this application.
     */
    @Override
    public void start(Stage stage)
    {
        spielesammlung = new Spielesammlung();
        wuerfel = new Wuerfel();
        // Create a Button or any control item
        Button myButton = new Button("Dreier Pasch");
        Button myButton2 = new Button("Statistik Dreier Pasch");
        // Create a new grid pane
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setMinSize(500, 800);
        pane.setVgap(10);
        pane.setHgap(10);

        //set an action on the button using method reference
        myButton.setOnAction(this::spielenDreierPasch_a);
        myButton2.setOnAction(this::spielenDreierPasch_s);
        
        // Add the button and label into the pane
        pane.add(myLabel, 1, 0);
        pane.add(myButton, 0, 0);
        pane.add(myLabel2, 1, 10);
        pane.add(myButton2, 0, 9);
        // JavaFX must have a Scene (window content) inside a Stage (window)
        Scene scene = new Scene(pane, 500,800);
        stage.setTitle("KeplerGames");
        stage.setScene(scene);
        
        // Show the Stage (window)
        stage.show();

    }

    /**
     * This will be executed when the button is clicked
     * It increments the count by 1
     */
    private void spielenDreierPasch_a(ActionEvent event)
    {
        // Counts number of button clicks and shows the result on a label
        int hResult = spielesammlung.spielenDreierPasch_automatisch();
        myLabel.setText("Du brauchtest " + Integer.toString(hResult) + " Würfe für den Pasch");
    }
    private void spielenDreierPasch_s(ActionEvent event)
    {
        int hResult = spielesammlung.spielenDreierPaschstatistik();
        myLabel2.setText("Im Durchschnitt brauchst du für einen Pasch " + Integer.toString(hResult) + " Würfe.");
    }
}