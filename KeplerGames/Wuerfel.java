
/**
 * Write a description of class Würfel here.
 * 
 * @author (Limbeck) 
 * @version (a version number or a date)
 */
public class Wuerfel
{
    private int anzahlSeiten; // die Anzahl der Wuerfelseiten
    
    public Wuerfel()
    {
        anzahlSeiten = 6;
    }
    
    public Wuerfel(int pAugenzahl)
    {
        anzahlSeiten = pAugenzahl;
    }
    
    public int getAnzahlSeiten()
    {
        return anzahlSeiten;
    }
    
    public void setAnzahlSeiten(int pAnzahl)
    {
        anzahlSeiten=pAnzahl; 
    }
    
    /**
     * Wuerfelt einmal und gib die Augenzahl zurueck.
     * Die Augenzahl ist groesser 0 und kleiner als anzahlSeiten
     */
    public int wuerfeln()
    {   int hAugenzahl;
        hAugenzahl= (int)(Math.random()*anzahlSeiten+1);
        return hAugenzahl;         
    }
        
}
