import java.util.Scanner;
/** Diese Klasse ist der Hauptteils des Spiels. Sie beinhaltet auch den Kern des Spiels void programm();
*/
  public class spiel extends spielbrett
{
    private int spieler = 1;//aktuell aktiver spieler
    Scanner sc = new Scanner(System.in);//Eingabe
    private String Einsetzer = "o";//Mit welchem Buchstaben soll das Feld geändert werden?
    private boolean gewonnen = false;//Wurde das Spiel gewonnen? Wichtig für logik()
    private boolean remis = false; // Oder ein Remis? auch für logik() und programm()
    private int hGewinner = 0; // wer hat gewonnen?
    /**
     * Constructor for objects of class spiel
     */
    public spiel()
    {
        zeigeSpielFeld(); // zeige das Spielfeld -> klasse spielbrett
    }
    /*
    * Getters und setters. selbsterklärend.
    */

    public boolean getRemis(){
        return remis;
    }
    public boolean getGewonnen(){
        return gewonnen;
    }
    public void setRemis(boolean remis){
        this.remis = remis;
    }
    public void setGewonnen(boolean gewonnen){
        this.gewonnen = gewonnen;
    }
    public int getSpieler(){
        return spieler;
    }
    public int gethGewinner(){
        return hGewinner;
    }
    /**
    * Diese Methode ist der Zug eines Spielers. Sie ist jedoch auch in dem CPU Spiel nutzbar.
    */
    public void macheZug(){
        zeigeSpielFeld(); // zeige feld
        System.out.println("\nIhr Zug, Spieler " + this.spieler + ". Bitte geben sie das FELD an, in dem sie setzen wollen.");
        int feld = sc.nextInt(); // nehme eingabe
        setzeFeld(feld, Einsetzer); // setze feld -> klasse spielbrett
        hGewinner = spieler; // wer gewinnt?
        // etwas ineffizient, aber es funktioniert
	// hier wird gesagt, wenn der spieler gerade dran ist, ändere das bitte und ändere auch den einsetzer.
	if(this.spieler == 1){
            this.spieler++;
            Einsetzer = "x";
        }
        else{
            this.spieler--;
            Einsetzer = "o";
        }
    }
    // sehr ähnlich wie die obere methode, nur dass hier eine zufällige zahl gezogen wird. Nur in programmCPU nutzbar!
    public void macheZugCPU(){
        zeigeSpielFeld();
        System.out.println("\n Der CPU ist an der Reihe. Er wird eine zufällige Zahl auserwählen.");
        int feld = (int)(Math.random() *9 +1); // zufällige zahl mit der Math bibliothek
        setzeFeld(feld, Einsetzer); // setze feld -> klasse spielbrett
        hGewinner = 2;
        if(this.spieler == 1){
            this.spieler++;
            Einsetzer = "x";
        }
        else{
            this.spieler--;
            Einsetzer = "o";
        }
    }
    /*
    Checkt ob der Spieler ein TicTacToe hat. (3 in einer Reihe)
    * Der Computer geht durch alle möglichen kombinationen, wenn an den jeweiligen indizes der selbe Buchstabe ist,ist das spiel gewonnen.
     */
    public void logik(){
        if(getSpielbrettX(0) == getSpielbrettX(1) && getSpielbrettX(0) == getSpielbrettX(2)){
            gewonnen = true;
        }
        else if (getSpielbrettX(3) == getSpielbrettX(4) && getSpielbrettX(3) == getSpielbrettX(5)){
            gewonnen = true;
        }
        else if (getSpielbrettX(6) == getSpielbrettX(7) && getSpielbrettX(6) == getSpielbrettX(8)){
            gewonnen = true;
        }
        else if (getSpielbrettX(0) == getSpielbrettX(3) && getSpielbrettX(0) == getSpielbrettX(6)){
            gewonnen = true;
        }
        else if (getSpielbrettX(1) == getSpielbrettX(4) && getSpielbrettX(1) == getSpielbrettX(7)){
            gewonnen = true;
        }
        else if (getSpielbrettX(2) == getSpielbrettX(5) && getSpielbrettX(2) == getSpielbrettX(8)){
            gewonnen = true;
        }
        else if (getSpielbrettX(0) == getSpielbrettX(4) && getSpielbrettX(0) == getSpielbrettX(8)){
            gewonnen = true;
        }
        else if (getSpielbrettX(2) == getSpielbrettX(4) && getSpielbrettX(2) == getSpielbrettX(6)){
            gewonnen = true;
        }
	// wenn alle felder belegt sind und nicht gewonnen wurde, ist es ein remis.
        if (getchangedFields() == 9 && gewonnen == false){
            remis = true;
        }
    }
    // hauptmethode -> übernimmt die Aufgabe des spielablaufs
    public void programm(){
        while(!gewonnen && !remis){ // solange nicht gewonnen oder remis wurde, wird dies ausgeführt
            macheZug();
            logik();
        }
        zeigeSpielFeld();
        if(remis == true){
            System.out.println("Unentschieden");
        }
        else {
        System.out.println("Glückwunsch, Spieler " + hGewinner +" Sie haben gewonnen!");
        }
    }
    // ähnlich wie oben, nur mit der methode macheZugCPU, daher eigens angelegt.
    public void programmCPU(){
        setComputerGegner(true); // für setzeFeld in Klasse spielbrett.
        while(!gewonnen && !remis){
            macheZug();
            logik();
            if(gewonnen){
                break;
            }
            macheZugCPU();
            logik();
        }
        zeigeSpielFeld();
        if(remis == true){
            System.out.println("Unentschieden");
        }
        else {
        System.out.println("Glückwunsch, Spieler " + hGewinner +" Sie haben gewonnen!");
        }
    }
}
