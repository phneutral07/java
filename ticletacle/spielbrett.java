import java.util.Scanner;
// Diese Klasse beinhaltet die "Stützen" des Spiels, wie beispielsweise setzeFeld()
public class spielbrett
{
    //wie viele felder wurden schon belegt?
    private int changedFields = 0;
    // ist der gegner ein computer? nützlich für setzeFeld???
    private boolean Computergegner = false;
    public String[] spielbrett = new String[9];
    Scanner sc = new Scanner(System.in);
    /**
     * Constructor for objects of class spielbrett
     */

    public spielbrett()
    {
        // populate the array with correct numbers
        for(int i = 0; i < spielbrett.length; i++){
            spielbrett[i] = Integer.toString(i+1);
        }
    }
    // diese methode zeigt das spielfeld in 3 zeilen, mit 3 for-schleifen.
    public void zeigeSpielFeld(){
        for(int i = 0; i<3;i++){
            System.out.print(spielbrett[i] + " ");
        }
        System.out.println("");
        for(int j = 3 ; j < 6; j++){
            System.out.print(spielbrett[j] + " ");
        }
        System.out.println("");
        for(int k = 6 ; k < 9; k++){
            System.out.print(spielbrett[k] + " ");
        }
        System.out.println("");
        System.out.println("");
    }
    // setzt einen MArker des jeweiligen spielers auf das feld
    //  nimmt zwei parameter, zuerst das feld indem der marker gesetzt werden soll
    // der zweite ist der spieler, besser gesagt der Einsetzer/Marker
    public void setzeFeld(int pFeld, String pSpieler){
        String hSpieler = pSpieler; // hilfsvariable, zur klarheit
        if(spielbrett[pFeld - 1] != "x" && spielbrett[pFeld-1] != "o"){ //erlaubt platzieren nur wenn feld leer
            spielbrett[pFeld - 1] = pSpieler; // ändere spielfeld zu einsetzer
            changedFields++; // brauchen wir für die if abfrage zum remis (spiel.java, Z: 102
        }
        else{
            if(!Computergegner){ // der computer würde nie eine zweite zahl eingeben, da er nur einmal eine zahl generiert. hiermit erlauben wir ihm mehrfach zahlen zu generieren. ausserdem kann der spieler hier ein zweiters mal wählen, und mithilfe von rekursion theoretisch unendlich oft das falsche feld eingeben, ohne das spiel abzubrechen.
            System.out.println("Belegtes Feld. Welches Feld wollen sie wirklich?");
            int feld = sc.nextInt();
            setzeFeld(feld, hSpieler);
            }
            else { // s.o
                if(changedFields != 9){
                int feld = (int)(Math.random() *9 +1);
                setzeFeld(feld, hSpieler);
            }
            }
        }
    }
    public int getchangedFields(){
        return changedFields;
    }
    public void setchangedFields(int changedFields){
        this.changedFields = changedFields;
    }
    // gibt das ganze spielbrett aus, in einer reihe und ohne leerzeichen.
    public void getSpielbrett(){
        for(int i = 0; i < spielbrett.length ; i++){
            System.out.print(spielbrett[i]);
        }
    }
    public String getSpielbrettX(int pX){
        return spielbrett[pX];
    }
    public void setComputerGegner(boolean bool){
	Computergegner = bool;
    }
}
