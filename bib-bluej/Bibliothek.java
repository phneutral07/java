public class Bibliothek {
    String nameBibliothek;
    String standort;
    buch[] buecher;
    buch buch1;
    buch buch2;
    buch buch3;
    buch buch4;
    buch buch5;

    /**
     * Konstruktor für Objekte der Klasse Bibliothek
     */
    public Bibliothek() {
        nameBibliothek = "Keplers Bibliothek";
        standort = "Ibbenbueren";
        buecher = new buch[5];
        buch1 = new buch("Eragon", "Christopher Paolini", 736);
        buch2 = new buch("Gregs Tagebuch", "Jeff Kinney", 224);
        buch3 = new buch("Harry Potter", "JK Rowling", 345);
        buch4 = new buch("Herr der Ringe", "Tolkien", 1000);
        buch5 = new buch("Informatik für Anfänger", "Informatiker", 50);
        buecher[0] = buch1;
        buecher[1] = buch2;
        buecher[2] = buch3;
        buecher[3] = buch4;
        buecher[4] = buch5;
    }

    /**
     * Gibt die Details zu jedem Buch aus
     */
    public void gibDetailsAllerBuecher(int book) {
        switch (book) {
            case 0:
                buch1.gibDetails();
                buch2.gibDetails();
                buch3.gibDetails();
                buch4.gibDetails();
                buch5.gibDetails();
                break;
            case 1:
                buch1.gibDetails();
                break;
            case 2:
                buch2.gibDetails();
                break;
            case 3:
                buch3.gibDetails();
                break;
            case 4:
                buch4.gibDetails();
                break;
            case 5:
                buch5.gibDetails();
                break;
        }
    }

    public void buchAusleihen(String pTitel, String pAusleiher) {
        if (pTitel == buch1.gibTitel()) {
            buch1.leiheAus(pAusleiher);
        } else if (pTitel == buch2.gibTitel()) {
            buch2.leiheAus(pAusleiher);
        } else if (pTitel == buch3.gibTitel()) {
            buch3.leiheAus(pAusleiher);
        } else if (pTitel == buch4.gibTitel()) {
            buch4.leiheAus(pAusleiher);
        } else if (pTitel == buch5.gibTitel()) {
            buch5.leiheAus(pAusleiher);
        }

    }

    public String gibNameDesGroeßtenBuches() {
        String pGroeßtesBuch = "";
        if (buch1.gibSeitenzahl() > buch2.gibSeitenzahl() && buch1.gibSeitenzahl() > buch3.gibSeitenzahl() && buch1.gibSeitenzahl() > buch4.gibSeitenzahl() && buch1.gibSeitenzahl() > buch5.gibSeitenzahl()) {
            pGroeßtesBuch = buch1.gibTitel();
        }
        if (buch2.gibSeitenzahl() > buch1.gibSeitenzahl() && buch2.gibSeitenzahl() > buch3.gibSeitenzahl() && buch2.gibSeitenzahl() > buch4.gibSeitenzahl() && buch2.gibSeitenzahl() > buch5.gibSeitenzahl()) {
            pGroeßtesBuch = buch2.gibTitel();
        }
        if (buch3.gibSeitenzahl() > buch2.gibSeitenzahl() && buch3.gibSeitenzahl() > buch1.gibSeitenzahl() && buch3.gibSeitenzahl() > buch4.gibSeitenzahl() && buch3.gibSeitenzahl() > buch5.gibSeitenzahl()) {
            pGroeßtesBuch = buch3.gibTitel();
        }
        if (buch4.gibSeitenzahl() > buch2.gibSeitenzahl() && buch4.gibSeitenzahl() > buch3.gibSeitenzahl() && buch4.gibSeitenzahl() > buch1.gibSeitenzahl() && buch4.gibSeitenzahl() > buch5.gibSeitenzahl()) {
            pGroeßtesBuch = buch4.gibTitel();
        }
        if (buch5.gibSeitenzahl() > buch2.gibSeitenzahl() && buch5.gibSeitenzahl() > buch3.gibSeitenzahl() && buch5.gibSeitenzahl() > buch4.gibSeitenzahl() && buch5.gibSeitenzahl() > buch1.gibSeitenzahl()) {
            pGroeßtesBuch = buch5.gibTitel();
        }
        return pGroeßtesBuch;
    }

    public String getNamedesGroeßtenBuchesARRAY() {
        String hpGroeßtesBuch = " ";
        int groeßteLaenge = 0;
        for (int i = 0; i < buecher.length; i++) {
            if(buecher[i].gibSeitenzahl() > groeßteLaenge){
                hpGroeßtesBuch = buecher[i].gibTitel();
                groeßteLaenge = buecher[1].gibSeitenzahl();
            }
        }
        return hpGroeßtesBuch;
    }
}
