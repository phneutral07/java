public class Main {
    public static void main(String[] args) {
        String x;
        x = "y";
        if(x.equals("y")){
            System.out.println(x);
        }
        else{
            System.out.print("equals false");
        }
        System.out.println(x.compareTo("y")); // gives out 0, cus they are equal
        System.out.println(x.compareTo("x")); // gives out higher number, cus larger unicode
        System.out.println(x.compareTo("z")); // negative, smaller unicode
    }
}
