import java.util.Scanner;
/**
 * Write a description of class spiel here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class spiel extends spielbrett
{
    private int spieler = 1;
    Scanner sc = new Scanner(System.in);
    private String Einsetzer = "o";
    private boolean gewonnen = false;
    private boolean remis = false;
    private boolean ende = false;
    private int hGewinner = 0;
    /**
     * Constructor for objects of class spiel
     */
    public spiel()
    {
        zeigeSpielFeld();
    }    

    public void macheZug(){
        zeigeSpielFeld();
        System.out.println("\nIhr Zug, Spieler " + this.spieler + ". Bitte geben sie das FELD an, in dem sie setzen wollen.");
        int feld = sc.nextInt();
        setzeFeld(feld, Einsetzer);
        hGewinner = spieler;
        if(this.spieler == 1){
            this.spieler++;
            Einsetzer = "x";
        }
        else{
            this.spieler--;
            Einsetzer = "o";
        }
    }
    public int getSpieler(){
        return spieler;
    }
    public void setSpieler(){
        if(spieler == 1){
            spieler = 2;
        }
        else {
            spieler = 1;
        }
    }
    /*
    Checkt ob der Spieler ein TicTacToe hat. (3 in einer Reihe)
     */
    public void logik(){
        int hZ = 0;
        if(getSpielbrettX(0) == getSpielbrettX(1) && getSpielbrettX(0) == getSpielbrettX(2)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(3) == getSpielbrettX(4) && getSpielbrettX(3) == getSpielbrettX(5)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(6) == getSpielbrettX(7) && getSpielbrettX(6) == getSpielbrettX(8)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(0) == getSpielbrettX(3) && getSpielbrettX(0) == getSpielbrettX(6)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(1) == getSpielbrettX(4) && getSpielbrettX(1) == getSpielbrettX(7)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(2) == getSpielbrettX(5) && getSpielbrettX(2) == getSpielbrettX(8)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(0) == getSpielbrettX(4) && getSpielbrettX(0) == getSpielbrettX(8)){
            this.gewonnen = true;
        }
        else if (getSpielbrettX(2) == getSpielbrettX(4) && getSpielbrettX(2) == getSpielbrettX(6)){
            this.gewonnen = true;
        } 
        else if (getchangedFields() == 9){
            this.remis = true;
        }
    }
    public void programm(){
         while(!gewonnen && !remis){
            macheZug();
            logik();
        }
        zeigeSpielFeld();
        if(remis = true){
            System.out.println("Unentschieden");
        }
        else {
        System.out.println("Glückwunsch, Spieler " + hGewinner +" Sie haben gewonnen!");
        }
    }
}
