import java.util.Scanner;

/**
 * Write a description of class spielbrett here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class spielbrett
{
    // instance variables - replace the example below with your own
    private int x;
    public int changedFields = 0;
    public String[] spielbrett = new String[9];
    Scanner sc = new Scanner(System.in);
    /**
     * Constructor for objects of class spielbrett
     */

    public spielbrett()
    {
        // populate the array with correct numbers
        for(int i = 0; i < spielbrett.length; i++){
            spielbrett[i] = Integer.toString(i+1);
        }
    }
    public void zeigeSpielFeld(){
        for(int i = 0; i<3;i++){
            System.out.print(spielbrett[i] + " ");
        }
        System.out.println("");
        for(int j = 3 ; j < 6; j++){
            System.out.print(spielbrett[j] + " ");
        }
        System.out.println("");
        for(int k = 6 ; k < 9; k++){
            System.out.print(spielbrett[k] + " ");
        }
    }
    public void setzeFeld(int pFeld, String pSpieler){
        String hSpieler = pSpieler;
        if(spielbrett[pFeld - 1] != "x" && spielbrett[pFeld-1] != "o"){
            spielbrett[pFeld - 1] = pSpieler;
            changedFields++;
        }
        else{
            System.out.println("Belegtes Feld. Welches Feld wollen sie wirklich?");
            int feld = sc.nextInt();
            setzeFeld(feld, hSpieler);
        }
    }
    public int getchangedFields(){
        return changedFields;
    }
    public void setchangedFields(int changedFields){
        this.changedFields = changedFields;
    }
    public void getSpielbrett(){
        for(int i = 0; i < spielbrett.length ; i++){
            System.out.print(spielbrett[i]);
        }
    }
    public String getSpielbrettX(int pX){
        return spielbrett[pX];
    }
}
