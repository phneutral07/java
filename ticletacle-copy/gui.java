

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Write a description of JavaFX class gui here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class gui extends Application
{
    // We keep track of the count, and label displaying the count:
    private spiel spiel = new spiel();
    private spielbrett spielbrett = new spielbrett();
    private Label grid1 = new Label("Sieger: ");
    private Label winner = new Label("");
    private Label spielerlabel = new Label(Integer.toString(spiel.getSpieler()));
    /**
     * The start method is the main entry point for every JavaFX application. 
     * It is called after the init() method has returned and after 
     * the system is ready for the application to begin running.
     *
     * @param  stage the primary stage for this application.
     */
    @Override
    public void start(Stage stage)
    {
        // Create a Button or any control item
        Button grid1b = new Button("1");
        Button grid2b = new Button("2");
        Button grid3b = new Button("3");
        Button grid4b = new Button("4");
        Button grid5b = new Button("5");
        Button grid6b = new Button("6");
        Button grid7b = new Button("7");
        Button grid8b = new Button("8");
        Button grid9b = new Button("9");
        Button done = new Button("Fertig mit dem Zug!");

        // Create a new grid pane
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setMinSize(300, 300);
        pane.setVgap(10);
        pane.setHgap(10);

        //set an action on the button using method reference
        done.setOnAction(this::changePlayer);
        grid1b.setOnAction(this::setzeSpielFeld1);
        grid2b.setOnAction(this::setzeSpielFeld2);
        grid3b.setOnAction(this::setzeSpielFeld3);
        grid4b.setOnAction(this::setzeSpielFeld4);
        grid5b.setOnAction(this::setzeSpielFeld5);
        grid6b.setOnAction(this::setzeSpielFeld6);
        grid7b.setOnAction(this::setzeSpielFeld7);
        grid8b.setOnAction(this::setzeSpielFeld8);
        grid9b.setOnAction(this::setzeSpielFeld9);
        // Add the button and label into the pane
        pane.add(grid1,  4, 1);
        pane.add(winner, 5, 1);
        pane.add(grid1b, 0, 0);
        pane.add(grid2b, 1, 0);
        pane.add(grid3b, 2, 0);
        pane.add(grid4b, 0, 1);
        pane.add(grid5b, 1, 1);
        pane.add(grid6b, 2, 1);
        pane.add(grid7b, 0, 2);
        pane.add(grid8b, 1, 2);
        pane.add(grid9b, 2, 2);
        pane.add(done,   8, 7);
        pane.add(spielerlabel,6, 7);

        // JavaFX must have a Scene (window content) inside a Stage (window)
        Scene scene = new Scene(pane, 400,200);
        stage.setTitle("JavaFX Example");
        stage.setScene(scene);

        // Show the Stage (window)
        stage.show();
    }
    public void changePlayer(ActionEvent event){
        spiel.setSpieler();
        spielerlabel.setText(Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld1(ActionEvent event){
        spielbrett.setzeFeld(0, Integer.toString(spiel.getSpieler()));
        
    }
    public void setzeSpielFeld2(ActionEvent event){
        spielbrett.setzeFeld(1, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld3(ActionEvent event){
        spielbrett.setzeFeld(2, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld4(ActionEvent event){
        spielbrett.setzeFeld(3, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld5(ActionEvent event){
        spielbrett.setzeFeld(4, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld6(ActionEvent event){
        spielbrett.setzeFeld(5, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld7(ActionEvent event){
        spielbrett.setzeFeld(6, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld8(ActionEvent event){
        spielbrett.setzeFeld(7, Integer.toString(spiel.getSpieler()));
    }
    public void setzeSpielFeld9(ActionEvent event){
        spielbrett.setzeFeld(8, Integer.toString(spiel.getSpieler()));
    }


}
