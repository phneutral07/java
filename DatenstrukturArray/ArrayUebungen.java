
/**
 * Write a description of class ArrayUebungen here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ArrayUebungen
{
    // declare array
    int[] testArray;
    double[] kontoStand;
    /**
     * Constructor for objects of class ArrayUebungen
     */
    public ArrayUebungen()
    {
        // initialize array
        testArray = new int[100];
        kontoStand = new double[30];
        for (int i = 0; i < 100; i++)
        {
          testArray[i] = (int)(Math.random() * (1000-1)+1);
        }
    }
    public void ausgebenVierteZahl()
    {
        for (int i = 0; i < testArray.length; i+=4)
        {
            System.out.println(testArray[i]);
        }
    }
    public void verdoppleWert()
    {
        for (int i = testArray.length-1; i > testArray.length-21; i-- ){
            testArray[i] *= 2;
        }
    }
    public void getMax()
    {
        int hHoechsterWert = 0;
        int hHoechsterWertIndex = 0;
        for (int i = 0; i < testArray.length; i++)
        {
            if (testArray[i] > hHoechsterWert) {
                hHoechsterWert = testArray[i];
                hHoechsterWertIndex = i;
            }
        }
        System.out.println("Der Index " + hHoechsterWertIndex + " ist am höchsten mit einem Wert von:");
        System.out.println(hHoechsterWert);
    }
    public void verdoppleDurchschnitt(int pZiel)
    {
        int hSumme = 0;
        for (int m = 0; m < testArray.length; m++)
        {
            hSumme += testArray[m];
        }
        int hDurchschnitt = hSumme / testArray.length;
        int hhDurchschnitt = hDurchschnitt;
        int hAnzahlMul = 0;
        while (hDurchschnitt < pZiel && hDurchschnitt*2 < pZiel)
        {
            System.out.println("Durchschnitt" + hDurchschnitt + "*2 = " + hDurchschnitt*2);
            hDurchschnitt *= 2;
            hAnzahlMul++;
        }
        //System.out.println("Durchschnitt" + hhDurchschnitt + "*2 = " + hhDurchschnitt*2 );
        System.out.println("Der Durchschnitt von " + hhDurchschnitt + " kann " + hAnzahlMul + " mal verdoppelt werden und liegt dann immernoch unter " + pZiel);
        System.out.println("Die Differenz von pZiel und dem aktuellen Wert beträgt: " + (pZiel - hDurchschnitt) );
    }
    public void sucheZahl(int pZahl)
    {
        boolean hErfolgreich = false;
        for(int i = 0; i < testArray.length && !hErfolgreich; i++)
        {
            if(testArray[i] == pZahl)
            {
                System.out.println("Der Gesuchte Wert ist an Index " + i);
                hErfolgreich = true;
            }
        }
        if(!hErfolgreich)
        {
            System.out.println("Nicht Gefunden");    
        }
    }

    public void berechneKontostand(double pAnfangsKontostand)
    {
        kontoStand[0] = pAnfangsKontostand;
        for(int i = 1; i < 30; i++)
        {
            kontoStand[i] = kontoStand[i-1];
            kontoStand[i] += kontoStand[i-1]*0.05;
            System.out.println("Im " + i + ". Jahr beträgt der Kontostand " + kontoStand[i] + " €");
        }

    }
}
